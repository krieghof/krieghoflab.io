---
title: "Run CLion Inside a Toolbox Container"
date: 2022-06-18T02:20:24+02:00
tags: ["Silverblue", "CLion"]
categories: ["Programming"]
draft: true
---

Today I thought it is time now to upgrade my main machine to Fedora 36 and maybe even try out [Fedora Silverblue](https://silverblue.fedoraproject.org/).
So I made a backup of my home folder, synced it with the NAS and installed Fedora Silverblue for the first time.
Silverblue uses `rpm-ostree` to provide a stable and immutable base OS. Therefore you should install new packages as [Flatpak](https://flatpak.org/) or use containers to separate them from the base system.

[Toolbox](https://containertoolbx.org/) is an awesome tool to create containers with an interactive terminal and an package manager. This becomes very handy when you are working on many different projects. Instead of cluttering your system with all the installed build dependencies, you simply install them inside a container. Even though I'm using this on Silverblue this should also work on other distros.

In this post I'm going to show you how to setup CLion and run it inside a Toolbox container. We also create a simple `.desktop` to integrate it with the Gnome Shell.
To be honest the process is pretty straight forward, but it took me a little while to figure out what works and what not.
I will also write a followup post where I show you how I use CLion with Meson Projects with the help of _Compilation Databases_.


# 1 -- Installing CLion
To make CLion accessible from the base OS and all Toolbox containers we have to install it somewhere in our `$HOME` directory. Toolbox has full access to our `$HOME` directory therefore we only have to install it once.
<>
The Flatpak version did not work for me, when I ran it inside the container it complained about missing build dependencies. Flatpak also restricts the access to `/usr/bin` this makes the integrated Terminal in CLion pretty useless.


To install CLion I used _Toolbox App_ from JetBrains. The names are unfortunately very similar.   
You can find the latest version on the [Toolbox App Website](https://www.jetbrains.com/toolbox-app/).
Download the `.tar.gz` version. The filename contains a `<version_number>` and might be different for you.

```
$ tar -xf "jetbrains-toolbox*.tar.gz"
$ cd jetbrains-toolbox-<version_number>
$ ./jetbrains-toolbox   
```
Toolbox will install itself and all tools into `~/.local/share/JetBrains/Toolbox/`.

## JetBrains Toolbox App

+ First you have to install CLion by simply clicking _Install_ 
+ Then change to the settings with the gear icon in the top right corner 
    + Scroll down and enable "Generate Shell script" and as location use `~/.local/bin` [^1]

<img src="/img/jetbrains-toolbox2.png" alt="img" style=";width:30%;border-radius:8px;border:1px solid grey;margin-bottom: 20px;margin-left: 20px;"/>
<img src="/img/jetbrains-toolbox.png" alt="img" style=";width:30%;border-radius:8px;border:1px solid grey;margin-bottom: 20px;margin-left: 20px;"/>


# 2 -- Run CLion Inside a Toolbox Container 

- With `toolbox create dev-container` you create a new container called _dev-container_, the first time you have to download a base image. In my case it was Fedora with ~500MB.

- Use `toolbox enter dev-container` to enter the container. Now you are inside the container and it is possible to install packages. Keep in mind the container is not fully isolated
from the host system e.g. it has read/write permissions for your home folder, so don't run any sketchy software inside them.

- Simply type `exit` to leave the container again.


Now it is possible to run CLion with `toolbox run --container dev-container ~/.local/bin/clion`.
This CLion instance will only use the libraries installed inside the Toolbox container.

## Gnome Shell Integration

Create `nano ~/.local/share/applications/clion-toolbox.desktop` with the following content and replace `<home>` in the 5th line with your actual home directory. You can run `echo $HOME` to get the correct directory.
```
[Desktop Entry]
Version=1.1
Type=Application
Name=CLion (on toolbox)
Icon=<home>/.local/share/JetBrains/Toolbox/apps/CLion/ch-0/.icon.svg
Exec=toolbox run --container dev-container ~/.local/bin/clion
Categories=Development;
```


[^1]: This step is not really necessary but it shortens the path to the `clion` binary and the directory does not change with the version
