---
title: "Working on a Meson Project with CLion"
date: 2022-06-18T09:18:39+02:00
tags: ["meson", "CLion", "gnome-disk-utility"]
categories: ["Programming"]
draft: true
---


In this post I'm going to show you how simple it is to setup CLion for a Meson Project with the help of _Compilation Databases_. 
You can also read about that approach on the [CLion Blog](https://blog.jetbrains.com/clion/2021/01/working-with-meson-in-clion-using-compilation-db/). Also vote on the JetBrains issue tracker maybe we get someday native [meson support](https://youtrack.jetbrains.com/issue/CPP-4926).

First we have to clone our project we want to work on and install all build dependencies. I'm going to do this in a Toolbox container like I explained in the last post.  
After that we have to generate the `compile_commands.json` with `meson`. Then we can open the project with CLion but we have to change the project root folder, add a __Custom Build Target_ and specify the correct application binary to Run/Debug within CLion. 


# Clone Project and build dependencies

We are going to use the [gnome-disk-utility](https://gitlab.gnome.org/GNOME/gnome-disk-utility) for this example.

First I'm going to create a new container with Toolbox and enter it. If you don't use Toolbox skip this part.
```
$ toolbox create gnome-disk-utility
$ toolbox enter gnome-disk-utility
```

Now I'm inside the container and can install all the dependencies with one single command `sudo dnf builddep gnome-disk-utility`   
I also clone the repository with `git clone git@gitlab.gnome.org:GNOME/gnome-disk-utility.git` into my home directory.

Now we have cloned the repo and installed all the dependencies required to build the application.

# Generate Compile Database 

Now we change in our new folder with `cd gnome-disk-utility` and run `meson builddir`.  
Meson will now build the project and generate the `compile_commands.json` inside the `builddir` directory.

# Open in CLion

First I'm going to exit the container and then run CLion inside the container. Again if you don't use Toolbox skip this part and simply open CLion like always.

```
$ exit
$ toolbox run --container gnome-disk-utility ~/.local/bin/clion
```

Now in CLion click on _Open_ and select the `compile_commands.json` in my case it is under `~/gnome-disk-utility/builddir/compile_commands.json.` hit _Ok_ and then _Open as Project_.

Now we have to change the project root directory to fix the _Project View_ on the left side.   
Click on _Tools_ -> _Compilation Database_ -> _Change Project Root_. In the newly opened window remove the `builddir` from the path and click the blue _OK_ button.

We now opened the Project successfully in CLion and code insight should already work. 

# Add Run/Debug Configuration
In the top right corner click on "Add Configuration..."-> "Add new..." -> "Custom Build Application"

Now you should have a window like that.
<img src="/img/clion-custom-build2.png" style="width:60%;border-radius:8px;border:1px solid grey;align:right;margin-bottom: 20px;margin-left: 20px">

+ Now click on "Configure Custom Build Target" -> "Add target"
    + On the right side of Build/Clean click on the button with the "...", to open the "External Tools" window
        + Here you click on the "+" symbol and again a new window opens. (we need to do this twice for Build and Clean)
            + Name: Build, Program: ninja, Working directory: $ProjectFileDir$
            + Name: Clean, Program: ninja, Arguments: clean, Working directory: $ProjectFileDir$
        

<img src="/img/clion-ninja.png" style="width:40%;border-radius:8px;border:1px solid grey;align:left;margin-bottom: 20px;margin-left: 20px">
<img src="/img/clion-ninja-clean.png" style="width:40%;border-radius:8px;border:1px solid grey;align:left;margin-bottom: 20px;margin-left: 20px">

After you added the two external tools, hit _OK_ and you should be back in the "Custom Build Target" Window.
Here make sure that every thing is correctly selected. Then hit _Apply_ and _OK_
<img src="/img/clion-build-target.png" style="width:40%;border-radius:8px;border:1px solid grey;align:left;margin-bottom: 20px;margin-left: 20px">

Now you are back at the first window, the same as in the first picture. As "Target" select our newly created target, in my case it is called "Unnamed". Then hit _Apply_ and _OK_. Now you should be able to compile the application for the first time. The compiled binary should be in the `builddir` usually under the `builddir/src` folder.

Now we have to specify the executable that we want to Run/Debug. In the top right corner click on "Unnamed" -> "Edit Configurations" to open the window again.
Click "..." on the right side of "Executable:" and select the compiled binary. In my case it is `~/gnome-disk-utility/builddir/src/disks/gnome-disks`.
Then hit _Apply_ and _OK_, congratulation you got it, now you can start working on the project like usual.


# Problems
1. When you change something in the `meson.build` or the `meson_options.txt` it has no effect.
    + Remove everything from the builddir except .idea and run `meson builddir` again. When you accidentally remove the .idea folder you have to do every step again. 
    + Maybe this has nothing to do with CLion, I'm still learning Meson