---
title: "Gnome-Disks -- Take Ownership"
date: 2021-07-18T01:52:35+02:00
tags: ["gnome-disk-utility"]
categories: ["Programming"]
draft: false
---

Like most of the actions *gnome-disks* can perform on your storage devices `TakeOwnership()` is also provided
by *udisks*. `TakeOwnership()` changes the ownership of the filesystem to the user (UID) and group (GID) of the calling user.
It is possible to call this method with the recursive mode to change the ownership for all subdirectories and files.
The operation is not available on *fat* and *ntfs* only for the typical unix filesystems.

<hr>

Merge Requests: [#48](https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/merge_requests/48) [#51](https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/merge_requests/51)  
Documentation: [TakeOwnership()](http://storaged.org/doc/udisks2-api/latest/gdbus-org.freedesktop.UDisks2.Filesystem.html#gdbus-method-org-freedesktop-UDisks2-Filesystem.TakeOwnership)  
Repositories: [gnome-disks](https://gitlab.gnome.org/GNOME/gnome-disk-utility) | [udisks](https://github.com/storaged-project/udisks)

## Basic Implementation   

<img src="/img/gnome-disks-volume-menu-ownership.png" style="margin-left:20px;margin-bottom: 20px;width:35%;border-radius:8px;float:right;border:1px solid grey;">

The first step was to add a new button at the appropriate position.
The volume menu contains already some similar actions like *Repair Filesystem* and *Check Filesystem* so *Take Ownership* would fit perfectly.
To change the layout I had to edit `src/disks/ui/volume-menu.ui` and add a new item right below the *Repair Filesystem* item.
The [merge request](https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/merge_requests/48/diffs#diff-content-837b0c3c2ae9316024cd8043bc300559fabed833)
includes a list of all the changes.

`TakeOwnership()` does not work for drives with a *ntfs* or *fat* filesystem so the best solution was to only enable
the action of the button with `g_simple_action_set_enabled ()` when the type of the filesystem is not *fat* or *ntfs*.

I created a new function and mapped it to the button. Now every time the button gets
pressed `on_volume_menu_item_take_ownership ()` gets called with our selected filesystem. When the operation is finished 
a callback function gets called and throws an error message if the operation was not successful.

<div style="clear:right;">

Here is the C code of the two new functions:
```c {linenos=table}
static void
fs_take_ownership_cb (UDisksFilesystem *filesystem,
                      GAsyncResult     *res,
                      GduWindow        *window)
{
  GError *error = NULL;
  // Throw error when the operation was not successful
  if (!udisks_filesystem_call_take_ownership_finish (filesystem, res, &error))
    {
      gdu_utils_show_error (GTK_WINDOW (window),
                            _("Error while taking filesystem ownership"),
                            error);

      g_error_free (error);
    }
}

static void
on_volume_menu_item_take_ownership (GSimpleAction *action,
                                    GVariant      *parameter,
                                    gpointer       user_data)
{
  GduWindow *window;
  UDisksObject *object;
  UDisksFilesystem *filesystem;

  window = GDU_WINDOW (user_data);
  object = gdu_volume_grid_get_selected_device (GDU_VOLUME_GRID (window->volume_grid));
  g_assert (object != NULL);

  filesystem = udisks_object_peek_filesystem (object);
  udisks_filesystem_call_take_ownership (filesystem,
                                         g_variant_new ("a{sv}", NULL),
                                         NULL,
                                         (GAsyncReadyCallback) fs_take_ownership_cb,
                                         window);
}
```

</div>

## Confirmation Dialog

All the other actions in the volume menu have a confirmation dialog with a basic description,
so the *Take Ownership* action should also have one. This dialog will also contain a check button to 
enable and disable the recursive mode.  
To archive this, I  created a new file `src/disks/ui/take-ownership-dialog.ui`.
The [file](https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/blob/6396518b4012c2335cdda80d59152f803f813ca4/src/disks/ui/take-ownership-dialog.ui)
contains all the information to define the visual of a confirmation dialog with a description, and a check button
for the recursion mode. 
I also had to change the code from earlier a little. The `fs_take_ownership_cb ()` now creates our message dialog with 
[GtkBuilder](https://developer.gnome.org/gtk3/stable/GtkBuilder.html) and `gtk_builder_new_from_resource ()`.

<img src="/img/gnome-disks-take-ownership-dialog.gif" style="width:60%;border-radius:8px;border:1px solid grey;float:right;margin-bottom: 20px;margin-left: 20px">

The recursive mode is a potential dangerous operation, so the ok button should get the `destructive-action` style
to visually warn the user by turning red.

To detect a button press, we have to connect to the *toogled* signal of the check button with `g_signal_connect ()`.
Now we can change the style according to the state of the button.

<hr style="clear:right" >

Special thanks to [Kai Lüke](https://kailueke.gitlab.io/) --- the maintainer of *gnome-disks* --- for giving me feedback and 
helping me to implement this feature.

If you want to learn more about programming for gnome --- check out [gtk](https://gtk.org/) or the [gnome developer center](https://developer.gnome.org/).