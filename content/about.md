---
title: "About"
date: 2021-07-18T21:38:52+08:00
menu: "main"
---

<img src="/me.jpg" alt="Picture of me" style="width:170px;height:170px;float:left;border-radius:50%;margin-right:10px;">
<p>
    My name is Manuel Wassermann and currently I study computer science at
    <a href="https://www.tuwien.at/en/tu-wien" title="TU Vienna" target="_blank">Technical University of Vienna</a>. I am interested in Linux Kernel development and userspace development. I also have a homelab with a Proxmox Cluster, where I spend all day with PfSense firewall rules. This is why I also got interested in FreeBSD and networking. <br>
</p>
<p>
    My desktop of choice is <a href="https://www.gnome.org/" title="GNOME" target="_blank">GNOME</a>
    and therefore I also like to spend my time on GTK application development.
    I also contribute regularly to the Gnome Project and the software I use. You can find my contributions on <a href="https://gitlab.gnome.org/krieghof" target="_blank">gitlab.gnome.org/krieghof</a>
    or here on the Blog. 
</p>
<p>
    You can contact me at <a href = "mailto:manuel.wassermann97@gmail.com">manuel.wassermann97@gmail.com</a>. 
    I also have a <a href="/key.asc" title="GPG Key" target="_blank">GPG key</a>
    if you wish to encrypt your message.
</p>
